﻿namespace BookStoreLibrary.Services
{
    public interface IUserService
    {
        string GetUserId();
    }
}