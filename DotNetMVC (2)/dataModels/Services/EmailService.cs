﻿
using BookStoreLibrary.Models;
using BookStoreLibrary.Services;
using Microsoft.Extensions.Options;
using System.Net.Mail;
using System.Net;
using System.Text;

namespace BookStoreLibrary.Services
{
    public class EmailService : IEmailService
    {
        private const string templatepath = @"EmailTemplate/{0}.html";
        private readonly SMTPConfigModel smtpConfigModel;
        public EmailService(IOptions<SMTPConfigModel> _smtpConfigModel)
        {
            smtpConfigModel = _smtpConfigModel.Value;
        }

        public async Task TestMail(UserEmailOptionsModel UserEmailOptions)
        {
            UserEmailOptions.Subject = UpdatePlaceHolders("{{name}} this is a text mail", UserEmailOptions.PlaceHolders);
            UserEmailOptions.Body = UpdatePlaceHolders(GetEmailBody("TestTemplate"),UserEmailOptions.PlaceHolders);

            await SendEmail(UserEmailOptions);
        }

        public async Task sendConfirmationEmail(UserEmailOptionsModel UserEmailOptions)
        {
            UserEmailOptions.Subject = UpdatePlaceHolders("hi  {{name}}, confirm your email id ", UserEmailOptions.PlaceHolders);
            UserEmailOptions.Body = UpdatePlaceHolders(GetEmailBody("EmailConfirm"), UserEmailOptions.PlaceHolders);

            await SendEmail(UserEmailOptions);
        }

        public async Task sendForgotPasswordEmail(UserEmailOptionsModel UserEmailOptions)
        {
            UserEmailOptions.Subject = UpdatePlaceHolders("hi  {{name}},reset your password ", UserEmailOptions.PlaceHolders);
            UserEmailOptions.Body = UpdatePlaceHolders(GetEmailBody("forgotPasswordTemplate"), UserEmailOptions.PlaceHolders);

            await SendEmail(UserEmailOptions);
        }

        private async Task SendEmail(UserEmailOptionsModel UserEmailOptions)
        {
            MailMessage mail = new MailMessage
            {
                Subject = UserEmailOptions.Subject,
                Body = UserEmailOptions.Body,
                From = new MailAddress(smtpConfigModel.SenderAddress, smtpConfigModel.SenderDisplayName),
                IsBodyHtml = smtpConfigModel.IsBodyHTML

            };
            foreach (var value in UserEmailOptions.ToEmails)
            {
                mail.To.Add(value);
            }

            NetworkCredential networkCredential = new NetworkCredential(smtpConfigModel.UserName, smtpConfigModel.Password);

            SmtpClient smtpClient = new SmtpClient
            {
                Host = smtpConfigModel.Host,
                Port = smtpConfigModel.Port,
                EnableSsl = smtpConfigModel.EnableSSl,
                UseDefaultCredentials = smtpConfigModel.UseDefaultCredentials,
                Credentials = networkCredential
            };
            mail.BodyEncoding = Encoding.Default;
            await smtpClient.SendMailAsync(mail);


        }

        private string UpdatePlaceHolders(string text, List<KeyValuePair<string,string>> PlaceHoldersKeyValue)
        {
            if(!string.IsNullOrEmpty(text)  && PlaceHoldersKeyValue != null)
            {
                foreach(KeyValuePair<string,string> value in PlaceHoldersKeyValue)
                {
                    if (text.Contains(value.Key))
                    {
                        text = text.Replace(value.Key, value.Value);
                    }
                }
            }
            return text;
        }


        private string GetEmailBody(string templateName)
        {
            var body = File.ReadAllText(string.Format(templatepath, templateName));
            return body;
        }
    }
}
