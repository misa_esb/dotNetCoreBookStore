﻿using BookStoreLibrary.Models;
namespace BookStoreLibrary.Services
{
    public interface IEmailService
    {
        Task TestMail(UserEmailOptionsModel UserEmailOptions);
        Task sendConfirmationEmail(UserEmailOptionsModel UserEmailOptions);
        Task sendForgotPasswordEmail(UserEmailOptionsModel UserEmailOptions);
    }
}