﻿using BookStoreLibrary.Repositories;
using BookStoreLibrary.Data;
using BookStoreLibrary.Models;

namespace BookStoreLibrary.Repositories
{
    public class LanguageRepository : ILanguageRepository
    {
        private readonly ApplicationDbContext _context = null;

        public LanguageRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<LanguageModel>> GetLanguages()
        {
            var data = await _context.BookLanguage.Select(x => new LanguageModel()
            {
                Id = x.Id,
                Name = x.Name,

            }).ToListAsync();
            return data;
        }
    }
}
