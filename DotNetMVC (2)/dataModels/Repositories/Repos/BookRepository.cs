﻿using BookStoreLibrary.Models;
using Microsoft.Extensions.Configuration;
using BookStoreLibrary.Data;
namespace BookStoreLibrary.Repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly ApplicationDbContext _context = null;
        private readonly IConfiguration _configuration;

        public BookRepository(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;

        }
        public async Task<List<BooksModal>> GetAllBooks()

        {
            var books = new List<BooksModal>();
            var data = await _context.Books.ToListAsync();
            if (data.Any() == true)
            {
                foreach (var book in data)
                {
                    books.Add(new BooksModal()
                    {
                        Id = book.Id,
                        Author = book.Author,
                        Title = book.Title,
                        Desc = book.Desc,
                        langName = book.langName,
                        LanguageId = book.LanguageId,
                        CreatedDate = DateTime.UtcNow,
                        UpdatedDate = DateTime.UtcNow,
                        Pages = book.Pages,
                        Category = book.Category,
                        myfield = book.myfield,
                        email = book.email,
                        coverImageUrl = book.coverImageUrl

                    });
                }
            }
        
            return books;
        }

        public async Task<List<BooksModal>> GetTopBooksAsync()

        {
            var books = new List<BooksModal>();
            return await _context.Books.Select(book => new BooksModal()
            {
                Id = book.Id,
                Author = book.Author,
                Title = book.Title,
                Desc = book.Desc,
                langName = book.langName,
                LanguageId = book.LanguageId,
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow,
                Pages = book.Pages,
                Category = book.Category,
                myfield = book.myfield,
                email = book.email,
                coverImageUrl = book.coverImageUrl
            }).Take(5).ToListAsync();

        }


        public async Task<int> AddNewBook(BooksModal model)
        {
            var newBook = new Books()
            {
                Title = model.Title,
                Desc = model.Desc,
                Author = model.Author,
                langName = model.langName,
                LanguageId = model.LanguageId,
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow,
                Pages = model.Pages,
                Category = model.Category,
                myfield = model.myfield,
                email = model.email,
                coverImageUrl = model.coverImageUrl,
                BookPdfUrl = model.BookPdfUrl
            };
            newBook.bookGallary = new List<bookGallary>();
            foreach (var file in model.Gallary)
            {
                newBook.bookGallary.Add(new bookGallary()
                {
                    Name = file.Name,
                    Url = file.Url,
                });
            }
            await _context.Books.AddAsync(newBook);
            await _context.SaveChangesAsync();
            return newBook.Id;
        }

        public async Task<BooksModal> GetBookById(int id)
        {
            var book = await _context.Books.Where(x => x.Id == id).Select(model => new BooksModal()
            {
                Title = model.Title,
                Desc = model.Desc,
                Author = model.Author,
                langName = model.langName,
                LanguageId = model.LanguageId,
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow,
                Pages = model.Pages,
                Category = model.Category,
                myfield = model.myfield,
                email = model.email,
                coverImageUrl = model.coverImageUrl,
                BookPdfUrl = model.BookPdfUrl,
                Gallary = model.bookGallary.Select(g => new GallaryModel()
                {
                    Url = g.Url,
                    Id = g.Id,
                    Name = g.Name
                }).ToList()
            }).FirstOrDefaultAsync();
            return book;
        }
        public List<BooksModal> SearchBooks(string title, string Author)
        {
            return null;
        }

        public string GetAppName()
        {
            return _configuration["appName"];
        }


    }
}
