﻿using Microsoft.Extensions.Options;
using BookStoreLibrary.Models;
namespace BookStoreLibrary.Repositories
{
    public class MessageRepository : IMessageRepository
    {
        private readonly AddNewBookConfig _AddNewConfiguration;

        public MessageRepository(IOptionsMonitor<AddNewBookConfig> AddNewBookAlert)
        {
            _AddNewConfiguration = AddNewBookAlert.CurrentValue;
        }
        public string GetName()
        {
            return _AddNewConfiguration.display;
        }
    }
}
