﻿using Microsoft.AspNetCore.Identity;
using BookStoreLibrary.Services;
using BookStoreLibrary.Models;
using BookStoreLibrary.Repositories;

namespace BookStoreLibrary.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IUserService userService;
        private readonly IEmailService _emailService;
        private readonly IConfiguration _configuration;



        public AccountRepository(UserManager<ApplicationUser> _userManager, SignInManager<ApplicationUser> _signInmanager, IUserService _userService, IEmailService emailService, IConfiguration configuration)
        {
            userManager = _userManager;
            signInManager = _signInmanager;
            userService = _userService;
            _emailService = emailService;
            _configuration = configuration;

        }


        public async Task<IdentityResult> CreateUserAsync(SignUpUserModel userModel)
        {
            var user = new ApplicationUser()
            {
                Email = userModel.email,
                UserName = userModel.email,
                firstName = userModel.firstName,
                lastName = userModel.lastName
            };
            var result = await userManager.CreateAsync(user, userModel.password);
            if (result.Succeeded)
            {
                await GenerateEmailConfirmationToken(user);
            }
            return result;
        }


        public async Task<ApplicationUser> GetUserByEmailIdAsync(string email)
        {
            return await userManager.FindByEmailAsync(email);

        }

        public async Task GenerateEmailConfirmationToken(ApplicationUser user)
        {
            var token = await userManager.GenerateEmailConfirmationTokenAsync(user);
            if (!string.IsNullOrEmpty(token))
            {
                await sendConfirmationMail(user, token);
            }
        }


        public async Task GenerateForgotPasswordToken(ApplicationUser user)
        {
            var token = await userManager.GeneratePasswordResetTokenAsync(user);
            if (!string.IsNullOrEmpty(token))
            {
                await sendForgotPasswordMail(user, token);
            }
        }

        public async Task<IdentityResult> ResetPasswordAsync(ResetPasswordModel model)
        {
            return await userManager.ResetPasswordAsync(await userManager.FindByIdAsync(model.UserId), model.Token, model.NewPassword);
        }


        public async Task<SignInResult> PasswordSignAsync(LogInUserModel logInModel)
        {
            var result = await signInManager.PasswordSignInAsync(logInModel.email, logInModel.Password, logInModel.RememberMe, true);
            return result;
        }

        public async Task userLogOutAsync()
        {
            await signInManager.SignOutAsync();
        }

        public async Task<IdentityResult> ChangePasswordAsync(ChangePasswordModel changePassword)
        {
            var userId = userService.GetUserId();
            var user = await userManager.FindByIdAsync(userId);
            var result = await userManager.ChangePasswordAsync(user, changePassword.CurrentPassword, changePassword.NewPassword);
            return result;
        }

        public async Task<IdentityResult> ConfirmEmailAsync(string uid, string token)
        {
            return await userManager.ConfirmEmailAsync(await userManager.FindByIdAsync(uid), token);
        }

        private async Task sendConfirmationMail(ApplicationUser user, string token)
        {
            string appDomain = _configuration.GetSection("Application:AppDomain").Value;
            string confirmationLink = _configuration.GetSection("Application:EmailConfirmation").Value;

            UserEmailOptionsModel model = new UserEmailOptionsModel
            {
                ToEmails = new List<string> { user.Email },
                PlaceHolders = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string,string>("{{name}}",user.firstName),
                    new KeyValuePair<string,string>("{{VerifyLink}}",string.Format(appDomain + confirmationLink,user.Id,token))
                }
            };
            await _emailService.sendConfirmationEmail(model);
        }

        private async Task sendForgotPasswordMail(ApplicationUser user, string token)
        {
            string appDomain = _configuration.GetSection("Application:AppDomain").Value;
            string ForgotPasswordLink = _configuration.GetSection("Application:ForgotPasswordEmail").Value;

            UserEmailOptionsModel model = new UserEmailOptionsModel
            {
                ToEmails = new List<string> { user.Email },
                PlaceHolders = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string,string>("{{name}}",user.firstName),
                    new KeyValuePair<string,string>("{{VerifyLink}}",string.Format(appDomain + ForgotPasswordLink,user.Id,token))
                }
            };
            await _emailService.sendForgotPasswordEmail(model);
        }
    }
}
