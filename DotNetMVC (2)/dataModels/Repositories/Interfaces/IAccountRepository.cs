﻿using Microsoft.AspNetCore.Identity;
using BookStoreLibrary.Models;

namespace BookStoreLibrary.Repositories
{
    public interface IAccountRepository
    {
        Task<IdentityResult> ChangePasswordAsync(ChangePasswordModel changePassword);
        Task<IdentityResult> ConfirmEmailAsync(string uid, string token);
        Task<IdentityResult> CreateUserAsync(SignUpUserModel userModel);
        Task GenerateEmailConfirmationToken(ApplicationUser user);
        Task GenerateForgotPasswordToken(ApplicationUser user);
        Task<ApplicationUser> GetUserByEmailIdAsync(string email);
        Task<SignInResult> PasswordSignAsync(LogInUserModel logInModel);
        Task userLogOutAsync();
        Task<IdentityResult> ResetPasswordAsync(ResetPasswordModel model);
    }
}