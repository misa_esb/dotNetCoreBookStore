﻿using BookStoreLibrary.Models;
namespace BookStoreLibrary.Repositories
{
    public interface IBookRepository
    {
        Task<int> AddNewBook(BooksModal model);
        Task<List<BooksModal>> GetAllBooks();
        Task<BooksModal> GetBookById(int id);
        List<BooksModal> SearchBooks(string title, string Author);
        Task<List<BooksModal>> GetTopBooksAsync();

        string GetAppName();
    }
}