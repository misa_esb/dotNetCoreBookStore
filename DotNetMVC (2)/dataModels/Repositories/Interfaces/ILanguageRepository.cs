﻿using BookStoreLibrary.Models;
namespace BookStoreLibrary.Repositories
{
    public interface ILanguageRepository
    {
        Task<List<LanguageModel>> GetLanguages();
    }
}