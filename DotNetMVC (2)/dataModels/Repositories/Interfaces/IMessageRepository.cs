﻿
namespace BookStoreLibrary.Repositories
{
    public interface IMessageRepository
    {
        string GetName();
    }
}