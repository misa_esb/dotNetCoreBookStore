﻿namespace BookStoreLibrary.Data
{
    public class Books
    {
        public int Id { get; set; }

        public string myfield { get; set; }

        public string email { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }

        public string Desc { get; set; }

        public string Category { get; set; }

        public int? Pages { get; set; }


        public string coverImageUrl { get; set; } = string.Empty;
        public string BookPdfUrl { get; set; } = string.Empty;


        public ICollection<bookGallary> bookGallary { get; set; } = new List<bookGallary>() { };



        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }



        public int LanguageId { get; set; } = 0;
        public string langName { get; set; } = string.Empty;

    }
}
