﻿namespace BookStoreLibrary.Data
{
    public class bookGallary
    { 
        public int Id { get; set; }
        public int BookId { get; set; }
        public string Name { get; set; }
         public string Url { get; set; }
        public Books book { get; set; }

    }
}
