﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using BookStoreLibrary.Models;

namespace BookStoreLibrary.Data
{

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
      
        public DbSet<Books> Books { get; set; }
        public DbSet<Language> BookLanguage { get; set; }
        public DbSet<bookGallary> BookGallary { get; set; }
    }

}
