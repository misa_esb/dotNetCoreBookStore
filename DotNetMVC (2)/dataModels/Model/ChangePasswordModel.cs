﻿namespace BookStoreLibrary.Models
{
    public class ChangePasswordModel
    {
        [Required(ErrorMessage ="please enter your current password"), Display(Name = "current password"),DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "please enter your new password"), Display(Name = "new password"), DataType(DataType.Password), Compare("confirmPassword",ErrorMessage ="passwrods do not match")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "please enter your confirm password"), Display(Name = "confirm password"), DataType(DataType.Password)]
        public string confirmPassword { get; set; }
    }
}
