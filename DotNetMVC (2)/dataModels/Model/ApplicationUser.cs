﻿global using System.ComponentModel.DataAnnotations;
global using System.ComponentModel.DataAnnotations.Schema;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.AspNetCore.Mvc.Rendering;
global using Microsoft.AspNetCore.Http;
global using Microsoft.Extensions.Configuration;

using Microsoft.AspNetCore.Identity;

namespace BookStoreLibrary.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string firstName { get; set; }
        public string lastName { get; set; }

    }
}
