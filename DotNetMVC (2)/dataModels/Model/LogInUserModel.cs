﻿namespace BookStoreLibrary.Models
{
    public class LogInUserModel
    {
        [Required(ErrorMessage ="please enter your email")]
        [EmailAddress(ErrorMessage ="please enter valid email address")]
        [Display(Name ="email")]
        public string email { get; set; }

        [Required(ErrorMessage ="please enter password")]
        [DataType(DataType.Password)]
        [Display(Name = "password")]
        public string Password { get; set; }

        public bool RememberMe { get; set; }


    }
}
