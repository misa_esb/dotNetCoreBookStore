﻿namespace BookStoreLibrary.Models
{
    public class forgotPasswordModel
    {
        [Required(ErrorMessage ="please enter your email address"),EmailAddress(ErrorMessage = "please enter valid email address"),Display(Name = "registered email")]
        public string Email { get; set; }
        public bool IsSent { get; set; } = false;
    }
}
