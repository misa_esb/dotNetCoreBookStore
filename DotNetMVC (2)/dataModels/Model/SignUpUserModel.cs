﻿namespace BookStoreLibrary.Models
{
    public class SignUpUserModel
    {
        [Required(ErrorMessage = "please enter your first name")]
        [Display(Name = "first name")]
        public string firstName { get; set; }

        [Required(ErrorMessage = "please enter your last name")]
        [Display(Name = "last name")]
        public string lastName { get; set; }


        [Required(ErrorMessage = "please enter your email address")]
        [EmailAddress(ErrorMessage = "please enter valid email address")]
        [Display(Name = "email")]

        public string email { get; set; }

        [Required(ErrorMessage = "please enter password")]
        [DataType(DataType.Password)]
        [Compare("confPassword", ErrorMessage = "confirm password and password does not match")]
        [Display(Name = "Password")]

        public string password { get; set; }

        [Required(ErrorMessage = "please enter confirm password")]
        [DataType(DataType.Password)]
        [Display(Name = "Re Enter password")]
        public string confPassword { get; set; }
    }
}
