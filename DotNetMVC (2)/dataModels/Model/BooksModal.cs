﻿

using BookStoreLibrary.Helpers;

namespace BookStoreLibrary.Models

{
    public class BooksModal
    {

        public BooksModal() { }

        [DataType(DataType.Date)]
        [Display(Name = "date of release")]
        [Required]
        public string myfield { get; set; }


        [DataType(DataType.EmailAddress)]
        [Display(Name = "your email address")]
        [Required]
        public string email { get; set; }
        public int Id { get; set; }


        //[StringLength(maximumLength: 100, MinimumLength = 2)]
        //[Required]
        [MyCustomValidation]
        public string Title { get; set; }

        [Required]
        [MaxLength(20)]
        [MinLength(2)]
        public string Author { get; set; }

        [Required]
        public string Desc { get; set; }

        [Required]
        public string Category { get; set; }

        [Required]
        public int? Pages { get; set; }


        [Required]
        [Display(Name = "choose the cover photo of your book")]
        public IFormFile CoverPhoto {   get; set; }
        public string coverImageUrl { get; set; } = string.Empty;


        [Required]
        [Display(Name = "upload the book pdf")]
        public IFormFile BookPdf { get; set; }
        public string BookPdfUrl { get; set; } = string.Empty;


        public IFormFileCollection GallaryImgs { get; set; }
        public List<GallaryModel> Gallary { get; set; } = new List<GallaryModel>() { };
      
       
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }


        [Required]
        public int LanguageId { get; set; } = 0;
        public string langName { get; set; } = string.Empty;
        // public LanguageModel LanguageModel { get; set; }
    }


}
