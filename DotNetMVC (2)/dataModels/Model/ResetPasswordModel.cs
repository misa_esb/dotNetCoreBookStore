﻿namespace BookStoreLibrary.Models
{
    public class ResetPasswordModel
    {
        [Required]
        public string UserId {get;set;}

        [Required]
        public string Token {get;set;}

        [Required(ErrorMessage = "please enter your new password"), Display(Name = "new password"), DataType(DataType.Password), Compare("confirmPassword", ErrorMessage = "passwrods do not match")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "please enter your confirm password"), Display(Name = "confirm password"), DataType(DataType.Password)]
        public string confirmPassword { get; set; }

        public bool IsReset { get; set; }
    }
}
