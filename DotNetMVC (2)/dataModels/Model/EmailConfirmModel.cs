﻿namespace BookStoreLibrary.Models
{
    public class EmailConfirmModel
    {
        public string Email { get; set; }
        public bool IsConfirm { get; set; }
        public bool EmailSent { get; set; }
        public bool EmailVerified { get; set; } = false;

    }
}
