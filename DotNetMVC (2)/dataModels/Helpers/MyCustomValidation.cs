﻿namespace BookStoreLibrary.Helpers
{
    public class MyCustomValidation : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value != null)
            {
                if (value.ToString().Contains("mvc")) 
                {
                    return ValidationResult.Success;
                }

            }

            return new ValidationResult("value is empty");

        }
    }
}
