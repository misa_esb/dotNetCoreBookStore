﻿using Microsoft.AspNetCore.Razor.TagHelpers;
namespace BookStoreLibrary.Helpers
{
    public class EmailTagHelper : TagHelper
    {
        public string MailAddr { get; set; }
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {  
            output.TagName = "a";
            output.Attributes.SetAttribute("href", "mailto:" + MailAddr);
            output.Content.SetContent(MailAddr);

            
        }



    }
}
