﻿using BookStoreLibrary.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace BookStoreLibrary.Helpers
{
    public class ApplicationUserClaimsPrincipleFactory : UserClaimsPrincipalFactory<ApplicationUser,IdentityRole>
    {
        public ApplicationUserClaimsPrincipleFactory(UserManager<ApplicationUser> userManager,RoleManager<IdentityRole> roleManager, IOptions<IdentityOptions> options) : base(userManager, roleManager, options) { }
        protected override async Task<ClaimsIdentity> GenerateClaimsAsync(ApplicationUser user)
        {
            var identity =  await base.GenerateClaimsAsync(user);
            identity.AddClaim(new Claim("UserFirstName",user.firstName??""));
            identity.AddClaim(new Claim("UserLastName", user.lastName ?? ""));
            return identity;

        }

    }
}
