﻿using Microsoft.AspNetCore.Mvc;
using BookStoreLibrary.Repositories;
using DotNetMVC.Components;
namespace DotNetMVC.Components
{
    public class TopBooksViewComponent : ViewComponent
    {
        private readonly IBookRepository _bookRepository;

        public TopBooksViewComponent(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var books = await _bookRepository.GetTopBooksAsync();
            return View(books);
        }

    }
}