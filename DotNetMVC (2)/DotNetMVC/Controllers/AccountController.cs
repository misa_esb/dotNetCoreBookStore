﻿using Microsoft.AspNetCore.Mvc;
using BookStoreLibrary.Repositories;
using Microsoft.AspNetCore.Authorization;

namespace DotNetMVC.Controllers
{

    public class AccountController : Controller

    {
        private readonly IAccountRepository _accountRepository;
        public AccountController(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        [HttpGet("Account/SignUp")]
        public IActionResult SignUp()
        {
            return View();
        }

        [HttpPost("Account/SignUp")]
        public async Task<IActionResult> SignUp(SignUpUserModel SignUpUser)
        {
            if (ModelState.IsValid)
            {
                var result = await _accountRepository.CreateUserAsync(SignUpUser);
                if (!result.Succeeded)
                {
                    foreach (var errorMessage in result.Errors)
                    {
                        ModelState.AddModelError("", errorMessage.Description);

                    }
                    return View(SignUpUser);

                }
                ModelState.Clear();
                return RedirectToAction("confirmMail", new { email = SignUpUser.email });
            }
            return View(SignUpUser);
        }

        [HttpGet("Account/Login")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost("Account/Login")]
        public async Task<IActionResult> Login(LogInUserModel LoginModel, string? returnUrl)
        {
            if (ModelState.IsValid)
            {
                var result = await _accountRepository.PasswordSignAsync(LoginModel);
                if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return LocalRedirect(returnUrl);

                    }
                    return RedirectToAction("Index", "home");
                }
                if (result.IsNotAllowed)
                {
                    ModelState.AddModelError("", "please confirm your mail before login");

                }
                if (result.IsLockedOut)
                {
                    ModelState.AddModelError("", "account blocked for some time. Please try again later");

                }
                else
                {
                    ModelState.AddModelError("", "Invalid credentials");

                }

            }
            return View(LoginModel);

        }

        [HttpGet("account/logout")]
        public async Task<IActionResult> Logout()
        {
            await _accountRepository.userLogOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet("account/changePassword")]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost("account/changePassword")]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel ChangePassword)
        {
            if (ModelState.IsValid)
            {
                var result = await _accountRepository.ChangePasswordAsync(ChangePassword);
                if (result.Succeeded)
                {
                    ViewBag.IsSuccess = true;
                    ModelState.Clear();
                    //await _accountRepository.userLogOutAsync();
                    //return RedirectToAction("Login", "Account");
                    return View(ChangePassword);

                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(ChangePassword);
        }

        [HttpGet("confirmMail")]
        public async Task<IActionResult> ConfirmMail(string uid, string token, string email)
        {
            EmailConfirmModel model = new EmailConfirmModel
            {
                Email = email,
            };

            if (!string.IsNullOrEmpty(uid) && !string.IsNullOrEmpty(token))
            {
                token = token.Replace(" ", "+");
                var result = await _accountRepository.ConfirmEmailAsync(uid, token);
                if (result.Succeeded)
                {
                    model.EmailVerified = true;
                }
            }
            return View(model);

        }


        [HttpPost("confirmMail")]
        public async Task<IActionResult> ConfirmMail(EmailConfirmModel model)
        {
            var user = await _accountRepository.GetUserByEmailIdAsync(model.Email);
            if (user != null)
            {
                if (user.EmailConfirmed == true)
                {
                    model.EmailVerified = true;
                    model.IsConfirm = true;

                    return View(model);
                }

            await _accountRepository.GenerateEmailConfirmationToken(user);
            model.EmailSent = true;
            ModelState.Clear();
            }

            else
            {
                ModelState.AddModelError("", "something went wrong");
            }
            return View(model);
        }


        [AllowAnonymous,HttpGet("account/forgotPassword")]
        public async Task<IActionResult> forgotPassword()
        {
           
            return  View();

        }



        [AllowAnonymous,HttpPost("account/forgotPassword")]
        public async Task<IActionResult> forgotPassword(forgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
               var user = await _accountRepository.GetUserByEmailIdAsync(model.Email);
                if(user!= null)
                {
                    await _accountRepository.GenerateForgotPasswordToken(user);
                }
                ModelState.Clear();
                model.IsSent = true;

            }
            return View(model);
        }

        [AllowAnonymous, HttpGet("account/ResetPassword")]
        public async Task<IActionResult> ResetPassword(string uid, string token)
        {
            ResetPasswordModel resetPasswordModel = new ResetPasswordModel
            {
                UserId = uid,
                Token = token
            };

            return View(resetPasswordModel);

        }

        [AllowAnonymous, HttpPost("account/ResetPassword")]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                model.Token = model.Token.Replace(" ", "+");
                var result = await _accountRepository.ResetPasswordAsync(model);
                if (result.Succeeded)
                {
                    ModelState.Clear();
                    model.IsReset = true;
                    return View(model);
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

            }
            return View(model);
        }
    }
}


