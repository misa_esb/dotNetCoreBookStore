﻿global using System.ComponentModel.DataAnnotations;
global using System.ComponentModel.DataAnnotations.Schema;
global using Microsoft.EntityFrameworkCore;
global using BookStoreLibrary.Data;
global using BookStoreLibrary.Models;
global using Microsoft.AspNetCore.Mvc.Rendering;
global using Microsoft.AspNetCore.Http;
global using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using BookStoreLibrary.Repositories;
using Microsoft.AspNetCore.Authorization;

namespace DotNetMVC.Controllers
{
    //[Route("Book")]
    [Route("[Controller]")]
    public class BookController : Controller
    {
        private readonly IBookRepository _bookRepository = null;
        private readonly ILanguageRepository _LanguageRepository = null;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public BookController(IBookRepository bookRepository,
            ILanguageRepository languageRepository,
            IWebHostEnvironment webHostEnvironment)
        {
            _bookRepository = bookRepository;
            _LanguageRepository = languageRepository;
            _webHostEnvironment = webHostEnvironment;
        }


        [HttpGet("GetAllBooks")]
        public async Task<ViewResult> GetAllBooks()
        {
            var data = await _bookRepository.GetAllBooks();
            return View("GetAllBooks", data);
        }

        [HttpGet("[Action]/{id:int:min(1)}")]
        public async Task<ViewResult> GetBook(int id)
        {
            var data = await _bookRepository.GetBookById(id);
            return View(data);
        }

        public List<BooksModal> SearchBooks(string bookName, string authorName)
        {
            return _bookRepository.SearchBooks(bookName, authorName);
        }

        [HttpGet("AboutUs")]
        public ViewResult AboutUs()
        {

            return View("AboutUs");

        }


        [HttpGet("AddNewBook")]
        [Authorize]
        public async Task<IActionResult> AddNewBook(bool isSuccess = false, int bookId = 0)
        {
            var model = new BooksModal();
            ViewBag.IsSuccess = isSuccess;
            ViewBag.bookId = bookId;
           // ViewBag.languages = new SelectList(await _LanguageRepository.GetLanguages(), "Id", "Name");
            return View();
        }


        [HttpPost("AddNewBook")]
        [Authorize]
        public async Task<IActionResult> AddNewBook(BooksModal booksModal)
        {
            if (ModelState.IsValid)
            {
                if (booksModal.CoverPhoto != null)
                {
                    string folderPath = "books/cover/";
                    booksModal.coverImageUrl = await UploadImages(folderPath, booksModal.CoverPhoto);
                }

                if (booksModal.BookPdf != null)
                {
                    string folderPath = "books/pdf/";
                    booksModal.BookPdfUrl = await UploadImages(folderPath, booksModal.BookPdf);
                }

                if (booksModal.GallaryImgs != null)
                {
                    string folderPath = "books/gallary/";
                    booksModal.Gallary = new List<GallaryModel>();
                    foreach (var imgs in booksModal.GallaryImgs)
                    {
                        var gallary = new GallaryModel()
                        {
                            Name = imgs.FileName,
                            Url = await UploadImages(folderPath, imgs)
                        };
                        booksModal.Gallary.Add(gallary);
                    }
                }

                int id = await _bookRepository.AddNewBook(booksModal);
                if (id > 0)
                {
                    return RedirectToAction(nameof(AddNewBook), new { isSuccess = true, bookId = id });
                }

            }

            ViewBag.IsSuccess = false;
           // ViewBag.languages = new SelectList(await _LanguageRepository.GetLanguages(), "Id", "Name");

            return View();
    }


    private async Task<string> UploadImages(string folderPath, IFormFile file)
    {

        folderPath += Guid.NewGuid().ToString() + "_" + file.FileName;

        string serverFolder = Path.Combine(_webHostEnvironment.WebRootPath, folderPath);

        await file.CopyToAsync(new FileStream(serverFolder, FileMode.Create));
        return (folderPath);
    }


}
}