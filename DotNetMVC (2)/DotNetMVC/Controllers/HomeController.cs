﻿using BookStoreLibrary.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using BookStoreLibrary.Repositories;
using BookStoreLibrary.Services;



//continue from video 110


namespace DotNetMVC.Controllers
{
    [Route("[Controller]/[Action]")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly AddNewBookConfig _AddNewConfiguration;
        private readonly  IMessageRepository _messageRepository;
        private readonly IUserService _userService;
        private readonly IEmailService _emailService ;



        public HomeController(ILogger<HomeController> logger,IOptionsSnapshot<AddNewBookConfig> AddNewBookAlert,IMessageRepository messageRepository, IUserService userService,IEmailService emailService)
        {
            _logger = logger;
            _AddNewConfiguration = AddNewBookAlert.Get("addBookAlert");
            _messageRepository = messageRepository;
            _userService = userService;
            _emailService = emailService;
        }

        [HttpGet("~/")]
        public async Task<IActionResult> Index()
        {
            //var userId = _userService.GetUserId();

            //bool isDisplay = _AddNewConfiguration.Istrue;
            //var MessageDisplay = _messageRepository.GetName();


            //var valueName = valueDetails.GetValue<string>("getName");
            //var valueBool = _configuration.GetValue<bool>("getDetails:getBool");
            //var valueInt = _configuration.GetSection("getDetails").GetValue<int>("getInt");

            //UserEmailOptionsModel model = new UserEmailOptionsModel
            //{
            //    ToEmails = new List<string> { "test@gmail.com" },
            //    PlaceHolders = new List<KeyValuePair<string, string>>()
            //    {
            //        new KeyValuePair<string,string>("{{name}}","Misa")
            //    }
            //};
            //await _emailService.TestMail(model);

            return View();
        }



       // [Route("Privacy-policies/{id}")]
        public IActionResult Privacy(int id)
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}